## Summary

<!--

Give context for what problem this issue is trying to prevent from happening again.

Provide a brief assessment of the risk (chance and impact) of the problem that this corrective action fixes, to assist with triage and prioritization.

-->
## Related Incident(s)
<!-- Note the originating incident(s) and link known related incidents/other issues 

/relate gitlab-com/gl-infra/production#ISSUE_ID
-->

Originating issue(s): gitlab-com/gl-infra/production#ISSUE_ID

## Desired Outcome/Acceptance criteria
<!--

How will you know that this issue is complete?

If you have any initial thoughts on implementation details e.g. what to do or not do, gotchas, edge cases etc, please share them while they are fresh in your mind.

-->

## Associated Services

<!--
Apply the appropriate services associate with this corrective action if appliable. 
 ~Service::SERVICE_NAME
-->

* 
## Corrective Action Issue Checklist

* [ ] link the incident(s) this corrective action arose out of
* [ ] give context for what problem this corrective action is trying to prevent from re-occurring
* [ ] assign a severity label (this is the highest sev of related incidents, defaults to 'severity::4')
* [ ] assign a priority (this will default to 'priority::4')

/label ~"team::Reliability" ~"ac::triage" ~"severity::4" ~"priority::4" ~"corrective action" 
